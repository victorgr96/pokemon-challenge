#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 12 13:07:26 2018

@author: victor
"""

import argparse
import os
from datetime import datetime
from random import randint
import time

import pandas as pd
import numpy as np
from sklearn import ensemble
import sklearn.metrics as metrics
from sklearn.externals import joblib
from sklearn.model_selection import train_test_split

data_dir = '/home/victor/data/datasets/mckinsey_ml_data/'
type_cmp = pd.read_csv(data_dir+"Weakness_Pokemon.csv",sep="|")
pk_types = pd.read_csv(data_dir+"All_Pokemons.csv",sep="|")
test_pks = pd.read_csv(data_dir+"AvailablePokemons.csv",sep="|")
sub_pks = pd.read_csv(data_dir+"Submission.csv",sep="|")

#Get the rates of weakness from pk types
def type_relation(battle,counter_battles):
    #First we obtain the names of the pokemons
    pk1_name = battle['Name_1']
    pk2_name = battle['Name_2']
    #Then we obtain teir corresponding type
    pk1_type = list(pk_types[pk_types.Name==pk1_name]['Type_1'])
    pk2_type = list(pk_types[pk_types.Name==pk2_name]['Type_1'])
    #Finally we get the relation of strength/weaknesses between them
    pk1_type_rel = type_cmp[type_cmp.Types==pk1_type[0]][pk2_type[0]]
    pk2_type_rel = type_cmp[type_cmp.Types==pk2_type[0]][pk1_type[0]]
    
    pk1_final_rel = pk1_type_rel.values[0]
    pk2_final_rel = pk2_type_rel.values[0]
    if counter_battles % 100 == 0:
        print('--------------------------')
        print(counter_battles)
       # print('PK1:',pk1_name)
       # print('PK2:',pk2_name)
        print(pk1_type,'vs',pk2_type)
        print('Relation t1 vs t2:',pk1_final_rel)
        print('Relation t2 vs t1:',pk2_final_rel)
        
    return  pk1_final_rel, pk2_final_rel

#Updates each battle applying type relation
def updateBattleType(battle,index,pk1_type_rel,pk2_type_rel,start_time):
    pk1_attack = battle['Attack_1']
    pk1_sp_attack = battle['Sp_Atk_1']
    pk2_attack = battle['Attack_2']
    pk2_sp_attack = battle['Sp_Atk_2']
    pk1_attack *= pk1_type_rel
    pk1_sp_attack *= pk1_type_rel
    pk2_attack *= pk2_type_rel
    pk2_sp_attack *= pk2_type_rel
    
    if index % 100 == 0:
        print('attack1',pk1_attack)
        print('attack2',pk2_attack)
        print('Working_time:',time.time() - start_time)

#Custom function for obtaining the output as a relative health [-1,1s]
def updateBattleResult(hp1,hp2,battle_result):
    if battle_result < 0:
        hp_output = battle_result / hp2
    else:
        hp_output = battle_result / hp1
    return hp_output


#Apply pokemon damage formula to obtain relative damaga
#Valid for special and physic damage
def calculateDamage(lvl1,atk1,df1,lvl2,atk2,df2):
    dmg1 = (0.01*atk1*(0.2*lvl1+1)) / (25*df2)
    dmg2 = (0.01*atk2*(0.2*lvl2+1)) / (25*df1)
    dmg = dmg1/dmg2
    return dmg

#updates chunk to create differences cols
#obtaining ratios to reduce data introduced to model
#we have to input names and types ?? Dont think so
def preprocessInput(chunk):
        new_chunk = chunk.assign(hp_rel  = lambda x: (x['HP_1'] / x['HP_2']))
        new_chunk = new_chunk.assign(dmg_phys_rel = lambda x: calculateDamage(x['Level_1'], x['Attack_1'], x['Defense_1'], x['Level_2'],x['Attack_2'], x['Defense_2']))
        new_chunk = new_chunk.assign(dmg_spec_rel = lambda x: calculateDamage(x['Level_1'], x['Sp_Atk_1'], x['Sp_Def_1'], x['Level_2'], x['Sp_Atk_2'], x['Sp_Def_2']))
        new_chunk = new_chunk.assign(speed_rel = lambda x: (x['Speed_1'] / x['Speed_2']))
        final_chunk_norm = new_chunk.loc[:,'hp_rel':]
        return final_chunk_norm

#obtain the output relative health   
def hpOutput(chunk):
    hp_output = pd.DataFrame({'hp_output':chunk.apply(lambda x: updateBattleResult(x['HP_1'], x['HP_2'], x['BattleResult']), axis=1)})
    output = np.ravel(hp_output.values)
    return output

     
#Gradient boosting model
def trainGrandientModel(total_battle):
    use_preprocessed = 0 #flag to use already preprocessed files (1) or not (0)
    date = "{:%d_%m}".format(datetime.now())
    filename = date+'model.joblib'
    random = randint(0,1000)
    params = {'n_estimators': 1000, 'max_depth': 4, 'min_samples_split': 2,
          'learning_rate': 0.005, 'verbose':2, 'loss': 'huber'} 
    if use_preprocessed == 0:
        start_time = time.time()
        for index,battle in total_battle.iterrows():
            #print(index)
            #gets relation of each pokemon to update characteristics
            pk1_type_rel,pk2_type_rel = type_relation(battle,index)
            updateBattleType(battle,index,pk1_type_rel,pk2_type_rel,start_time)
            #after updating charactericts, create new cols with differences
        final_input = preprocessInput(total_battle)
        final_output = hpOutput(total_battle)
        np.save(data_dir+"battle_input.npy", final_input)
        np.save(data_dir+"battle_output.npy", final_output)
    
    else:
        #Loads preprocessed dataset from file
        final_input  = np.load(data_dir+"battle_input.npy")
        final_output = np.load(data_dir+"battle_output.npy")
    #Split dataset   
    X_train, X_test, y_train, y_test = train_test_split(final_input, final_output, test_size=0.1, random_state=42)
    model = ensemble.GradientBoostingRegressor(**params)
    #Train model and save to file
    model.fit(X_train, y_train)
    joblib.dump(model,str(random)+'_'+filename)
    y_pred = model.predict(X_test)
    #Measure scores
    r_score = metrics.r2_score(y_test,y_pred)
    mean_abs = metrics.mean_absolute_error(y_test,y_pred)
    print('r_score:',r_score)
    print('mean abs:',mean_abs)


def normalizePred(prediction):
#    max_value = max(abs(prediction.min()),abs(prediction.max()))
#    prediction /= max_value
    df_pred = pd.DataFrame({'HP':prediction,'PK_Price':test_pks['Price_1']})
    df_pred = df_pred.assign(HP_Price_Rate = lambda x: (x['HP'] / x['PK_Price']))
    df_pred_sorted = df_pred.sort_values(by='HP', ascending=False)
    return df_pred_sorted

def filterPred(ord_pred,threshold):
    filter_pred = ord_pred[(ord_pred.HP > threshold) & (ord_pred.PK_Price < 2500)]
    return filter_pred

#Iterate over the pokemons who have to win their battle
#Get the combination that maximizes the mean average health
def getBestAverage3(predictions_pks):
    print('Beginning combinations ...')
    final_idx4 = 0
    final_idx5 = 0
    final_idx6 = 0
    hp_max = 0.00000
    #Max budget for three pokemons
    precio_max = 2763 #3500-205-265-267
    counter_it = 0
    total_it = len(predictions_pks[0].index)
    for idx4, pk4 in predictions_pks[0].iterrows():
        counter_it += 1
        print(counter_it,'iteration of',total_it)
        for idx5, pk5 in predictions_pks[1].iterrows():
            for idx6, pk6 in predictions_pks[2].iterrows():
                hp_media = (pk4['HP'] + pk5['HP']+ pk6['HP']) / 3
                precio = pk4['PK_Price'] + pk5['PK_Price'] +  pk6['PK_Price'] 
                if(hp_media > hp_max and precio < precio_max):
                    if(idx5 != idx6):
                        hp_max = hp_media
                        final_idx4 = idx4
                        final_idx5 = idx5
                        final_idx6 = idx6
                        print('\n'.join([str(idx4),str(idx5),str(idx6)]))
    return hp_max, final_idx4,final_idx5,final_idx6  

def getBestAverage2(predictions_pks):
    print('Beginning combinations ...')
    final_idx4 = 0
    final_idx5 = 0
    hp_max = 0.00000
    #Max budget for three pokemons
    precio_max = 2489 #3500-205-265-267
    counter_it = 0
    total_it = len(predictions_pks[0].index)
    for idx4, pk4 in predictions_pks[0].iterrows():
        counter_it += 1
        print(counter_it,'iteration of',total_it)
        for idx5, pk5 in predictions_pks[1].iterrows():
                hp_media = (pk4['HP'] + pk5['HP']) / 2
                precio = pk4['PK_Price'] + pk5['PK_Price']
                if(hp_media > hp_max and precio < precio_max):
                    if(idx4 != idx5):
                        hp_max = hp_media
                        final_idx4 = idx4
                        final_idx5 = idx5
                        print('\n'.join([str(idx4),str(idx5)]))
    return hp_max, final_idx4,final_idx5

#Prediction mode
def prediction(model):
     sub_pks.pop('SelectedPokemonID')
     test_pks.pop('SelectedPokemonID')
     filter_pred_array = []
     ordered_pred_array = []
     threshold_array = [0.7,0,0,-1,0,0]
     #Compare each submision pokemon to the complete battle
     for index in range(len(sub_pks.index)):
         pk_target = sub_pks.loc[index]
         pk_target = pd.DataFrame(pk_target).T
         #extend the current pokemon to fight to each pokemon available
         pk_target_df = pd.concat([pk_target]*1440, ignore_index=True)
         battle = pd.concat([test_pks,pk_target_df], axis=1)
         final_battle = preprocessInput(battle)
         prediction = model.predict(final_battle)
         ord_pred = normalizePred(prediction)
         filter_pred = filterPred(ord_pred,threshold_array[index])
         ordered_pred_array.append(ord_pred)
         filter_pred_array.append(filter_pred)
     return filter_pred_array,ordered_pred_array

def testMetrics(model):
    final_input  = np.load(data_dir+"battle_input.npy")
    final_output = np.load(data_dir+"battle_output.npy")
    #Split dataset   
    X_train, X_test, y_train, y_test = train_test_split(final_input, final_output, test_size=0.1, random_state=42)
    y_pred = model.predict(X_test)
    #Measure scores
    r2_score = metrics.r2_score(y_test,y_pred)
    mean_abs = metrics.mean_absolute_error(y_test,y_pred)
    print('r2_score:',r2_score)
    print('mean abs:',mean_abs)
    
    
if __name__ == "__main__":
    
    parser = argparse.ArgumentParser()
    parser.add_argument('mode',type=str,help='Choose train or predict mode')
    args= parser.parse_args()
    #Train mode
    if args.mode == 'train':
        total_battles = pd.read_csv(data_dir+"Battle_Results.csv",sep="|")
        trainGrandientModel(total_battles)
    elif args.mode == 'test':
        model = joblib.load('final_model.joblib')
        testMetrics(model)
    elif args.mode =='predict':
        #Load model
        model = joblib.load('104_22_10model.joblib')
        print(model,'\n')
        #First filter of possible candidates
        filter_pred_array,ordered_pred_array = prediction(model)
        final_ids=[]
        list_average2 = []
        list_average3 = []
        final_mean = 0.0
        mean_hp2,mean_hp3 = 0,0
        caterpie_th_price = 539
        stamped_pks = 0
        #Obtain the average
        if stamped_pks != 0:
            if stamped_pks == 4:
                list_average2.append(filter_pred_array[0])
                list_average2.append(filter_pred_array[5])
                hp_max_2,id1,id2 = getBestAverage2(list_average2)
                mean_hp_2 = (hp_max_2*2) / 6
                final_ids.append([id1,id2])
            elif stamped_pks == 3:
                #Caterpie battle thresholded by optimal value
                df_caterpie = filter_pred_array[0]
                df_caterpie_filter = df_caterpie[(df_caterpie.PK_Price < caterpie_th_price)]
                #Append battles whose pokemon wont be discarded
                list_average3.append(df_caterpie_filter)
                list_average3.append(filter_pred_array[4])
                list_average3.append(filter_pred_array[5])
                #Obtain the average
                hp_max_3,id12,id52,id62 = getBestAverage3(list_average3)
                mean_hp_3 = (hp_max_3*3) / 6
                final_ids.append([id12,id52,id62])
                
            if mean_hp_2 > mean_hp_3:
                final_ids.append([id1,id2])
            else:
                final_ids.append([id12,id52,id62])
            
        
